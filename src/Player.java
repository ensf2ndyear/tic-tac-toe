import java.util.Scanner;

/**
 * Creates the player object and allows each player to have their turn
 * @author Sadat Islam
 * @date 1/31/2017
 */
public abstract class Player {

    /**
     * name of the player
     */
    protected String name;
    /**
     * board for the game
     */
    protected Board board;
    /**
     * opponent object
     */
    protected Player opponent;
    /**
     * mark for the player, x or o
     */
    protected char mark;

    /**
     * Constructs object with name and a mark
     * @param name name of the user
     * @param mark mark/stamp
     */
    protected Player(String name, char mark){
        this.mark = mark;
        this.name = name;
    }

    /**
     * Get's the name of the Player
     * @return name variable
     */
    protected String getName(){
        return name;
    }

    /**
     * Starts the players turn and checks if the other player already won or if the board is full
     */
    abstract protected void play();

    /**
     * Allows player to select their next move
     */
    abstract protected void makeMove();

    /**
     * Set's the players opponent
     * @param x player object of the opponent
     */
    protected void setOpponent( Player x){
        opponent = x;

    }

    /**
     * Board object to be played on
     * @param theBoard board object
     */
    protected void setBoard ( Board theBoard){
        board = theBoard;

    }

}
