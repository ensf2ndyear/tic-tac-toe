/**
 * Created by Sadat Msi on 2/4/2017.
 */
public class RandomPlayer extends Player {

    /**
     * Constructs a instance of RandomPlayer
     * @param name name of player
     * @param mark 'X' or 'O' for tic tac toe game
     */
    protected RandomPlayer(String name, char mark){
        super(name, mark);
    }


    /**
     * Chooses a random free spot to put it's mark
     */
    protected void makeMove(){
        randomMove();

    }

    protected void randomMove(){
        int row, col;
        RandomGenerator rand = new RandomGenerator();

        while(true) {

            row = rand.discrete(0,2);
            col = rand.discrete(0,2);
            if (board.getMark(row, col) == ' '){
                board.addMark(row, col, mark);
                break;
            }
        }
    }

    protected void play(){
        if( board.xWins() == false && board.oWins() == false && board.isFull() == false)
        {
            makeMove();
            board.display();
            board.checkWinner(mark);
            opponent.play();

        }
        else
        {
            System.out.print("THE GAME IS OVER: ");
            if( board.isFull() )
                System.out.println(" The game is a tie!");
            else if ( board.checkWinner(mark) == 1 )
                System.out.printf(" %s is the winner!", name);
            else
                System.out.printf(" %s is the winner!", opponent.getName());

            System.exit(0);

        }


    }

}
