/**
 * Sets up the player and their opponents and imitates the game
 * @author Sadat Islam
 * @date 1/31/2017
 */
public class Referee {
    /**
     * Contains the two player objects who will play each other
     */
    private Player xPlayer;
    private Player oPlayer;
    /**
     * contains the board on which the game will be played on
     */
    private Board board;

    /**
     * Set's up each player against each other, runs the display, and initiates player 1 to go
     */
    public void runTheGame(){
        xPlayer.setOpponent( oPlayer );
        oPlayer.setOpponent( xPlayer );
        board.display();
        xPlayer.play();


    }

    /**
     * Sets board
     * @param board board object
     */
    public void setBoard( Board board){
        this.board = board;

    }

    /**
     * Sets a player
     * @param oPlayer Player object
     */
    public void setoPlayer( Player oPlayer){
        this.oPlayer = oPlayer;

    }

    /**
     * Sets another player
     * @param xPlayer player object
     */
    public void setxPlayer( Player xPlayer){
        this.xPlayer = xPlayer;

    }

}
