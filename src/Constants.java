/**
 * Constants for the three types of marks on the board
 */
public interface Constants {
	static final char SPACE_CHAR = ' ';
	static final char LETTER_O = 'O';
	static final char LETTER_X = 'X';
}
