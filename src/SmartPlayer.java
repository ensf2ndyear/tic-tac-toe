/**
 * Created by Sadat Msi on 2/4/2017.
 */
public class SmartPlayer extends BlockingPlayer {

    /**
     * Constructor for object SmartPlayer
     * @param name name of player
     * @param mark X or O for tic tac toe game
     */
    protected SmartPlayer(String name, char mark){
        super(name, mark);
    }

    /**
     * Makes the move for the Player
     */
    protected void makeMove(){
        smartMove();

    }

    /**
     * Transverses the board and looks for a win
     */
    protected void smartMove(){
        int row, col;
        for( row = 0; row < 3; row++)
            for ( col = 0; col < 3; col++)
                if(board.getMark(row, col) == ' ')
                    if(testForWin(row,col)) {
                        board.addMark(row, col, mark);
                        return;
                    }



        blockingMove();
    }

    /**
     * Checks to see if playing it here will cause a win
     * @param row
     * @param col
     * @return true if it will lead to a win, false otherwise
     */
    protected boolean testForWin(int row, int col){
        board.addMark(row, col, mark);
        int temp = board.checkWinner(mark);
        if(temp == 1)
        {
            board.addMark(row, col, ' ');
            board.markCountDecrease(2);
            return true;
        }

        board.addMark(row, col, ' ');
        board.markCountDecrease(2);
        return false;
    }

}
