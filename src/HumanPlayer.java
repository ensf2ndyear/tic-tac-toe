import java.util.Scanner;

/**
 * Created by Sadat Msi on 2/4/2017.
 */
public class HumanPlayer extends Player {

    /**
     * Constructor for object BlockingPlayer
     * @param name name of player
     * @param mark X or O for tic tac toe game
     */
    protected HumanPlayer(String name, char mark){
        super(name, mark);
    }


    protected void makeMove() {
        int row, col;

        Scanner in = new Scanner(System.in);

        while (true)
        {


            while (true) {
                System.out.println(name + ", what row should your next " + mark + " be placed in?");
                row = in.nextInt();
                if (row < 0 || row > 2)
                    System.out.println("Invalid row, please try again");
                else
                    break;
            }

            while (true) {
                System.out.println(name + ", what column should your next " + mark + " be placed in?");
                col = in.nextInt();
                if (col < 0 || col > 2)
                    System.out.println("Invalid column, please try again");
                else
                    break;
            }
            if (board.getMark(row, col) == ' ') {
                board.addMark(row, col, mark);
                break;
            } else
                System.out.println("That space is already taken, please select another index.");
        }
    }

    protected void play(){
        if( board.xWins() == false && board.oWins() == false && board.isFull() == false)
        {
            makeMove();
            board.display();
            board.checkWinner(mark);
            opponent.play();

        }
        else
        {
            System.out.print("THE GAME IS OVER: ");
            if( board.isFull() )
                System.out.println(" The game is a tie!");
            else if ( board.checkWinner(mark) == 1 )
                System.out.printf(" %s is the winner!", name);
            else
                System.out.printf(" %s is the winner!", opponent.getName());

            System.exit(0);

        }


    }
}

