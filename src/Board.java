/**
 *  Set's up the board for a tic tack toe game
 *  @author ENSF 409 Instructors
 *  @version 1.0
 *
 */
public class Board implements Constants {
    /**
     * Coordinates of the tic tack toe board
     */
    private char theBoard[][];

    /**
     * Number of moves played
     */
	private int markCount;

    /**
     * Constructs the board with all points having a ' ' in them
     */
	public Board() {
		markCount = 0;
		theBoard = new char[3][];
		for (int i = 0; i < 3; i++) {
			theBoard[i] = new char[3];
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		}
	}

	/**
	 * Decrements the value of markCount
	 */
	public void markCountDecrease(int i){
		markCount -= i;
	}

    /**
     * Returns the character in a coordinate
     * @param row index of row
     * @param col index of collumn
     * @return returns what is in the coordinate
     */
	public char getMark(int row, int col) {
		return theBoard[row][col];
	}

    /**
     * States if the board is full
     * @return true if all moves are done, false if there are more moves to be played
     */
	public boolean isFull() {
		return markCount == 9;
	}

    /**
     *  Checks to see if x has three in a row
     * @return xWins the game is return is true
     */
	public boolean xWins() {
		if (checkWinner(LETTER_X) == 1)
			return true;
		else
			return false;
	}

    /**
     * Checks to see if o has three in a row
     * @return oWins if this is true
     */
	public boolean oWins() {
		if (checkWinner(LETTER_O) == 1)
			return true;
		else
			return false;
	}

    /**
     * Prints the board on the console
     */
	public void display() {
		displayColumnHeaders();
		addHyphens();
		for (int row = 0; row < 3; row++) {
			addSpaces();
			System.out.print("    row " + row + ' ');
			for (int col = 0; col < 3; col++)
				System.out.print("|  " + getMark(row, col) + "  ");
			System.out.println("|");
			addSpaces();
			addHyphens();
		}
	}

    /**
     * Adds a mark to the board
     * @param row row index
     * @param col column index
     * @param mark either x or o
     */
	public void addMark(int row, int col, char mark) {
		
		theBoard[row][col] = mark;
		markCount++;
	}

    /**
     * Fills the array with ' '
     */
	public void clear() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		markCount = 0;
	}

    /**
     * Checks all cases for char mark to see if it has won
     * @param mark mark to be checked
     * @return 1 if a result has been made, 0 if not
     */
	int checkWinner(char mark) {
		int row, col;
		int result = 0;

		for (row = 0; result == 0 && row < 3; row++) {
			int row_result = 1;
			for (col = 0; row_result == 1 && col < 3; col++)
				if (theBoard[row][col] != mark)
					row_result = 0;
			if (row_result != 0)
				result = 1;
		}

		
		for (col = 0; result == 0 && col < 3; col++) {
			int col_result = 1;
			for (row = 0; col_result != 0 && row < 3; row++)
				if (theBoard[row][col] != mark)
					col_result = 0;
			if (col_result != 0)
				result = 1;
		}

		if (result == 0) {
			int diag1Result = 1;
			for (row = 0; diag1Result != 0 && row < 3; row++)
				if (theBoard[row][row] != mark)
					diag1Result = 0;
			if (diag1Result != 0)
				result = 1;
		}
		if (result == 0) {
			int diag2Result = 1;
			for (row = 0; diag2Result != 0 && row < 3; row++)
				if (theBoard[row][3 - 1 - row] != mark)
					diag2Result = 0;
			if (diag2Result != 0)
				result = 1;
		}
		return result;
	}

    /**
     * Prints the headings of the columns
     */
	void displayColumnHeaders() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|col " + j);
		System.out.println();
	}

    /**
     * Adds the skeleton of the board
     */
	void addHyphens() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("+-----");
		System.out.println("+");
	}

    /**
     * Fills in the empty spaces for the board
     */
	void addSpaces() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|     ");
		System.out.println("|");
	}
}
