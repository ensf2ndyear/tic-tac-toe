/**
 * Created by Sadat Msi on 2/4/2017.
 */
public class BlockingPlayer extends RandomPlayer {

    /**
     * Constructor for object BlockingPlayer
     * @param name name of player
     * @param mark X or O for tic tac toe game
     */
    protected BlockingPlayer(String name, char mark){
        super(name, mark);
    }

    /**
     * Makes the move for the player
     */
    protected void makeMove() {
        blockingMove();
    }

    /**
     * Transverses the board and sees where it needs to block
     */
    protected void blockingMove(){
        int row, col;
        for( row = 0; row < 3; row++)
            for ( col = 0; col < 3; col++)
                if(board.getMark(row, col) == ' ')
                    if(testForBlocking(row,col)) {
                        board.addMark(row, col, mark);
                        return;
                    }

        randomMove();

    }

    /**
     * Checks if this point will lead to a loss on the next turn
     * @param row
     * @param col
     * @return true if it will be a loss, false otherwise
     */
    protected boolean testForBlocking(int row, int col){
        board.addMark(row, col, opponent.mark);
        int temp = board.checkWinner(opponent.mark);
        if(temp == 1)
        {
            board.addMark(row, col, ' ');
            board.markCountDecrease(2);
            return true;
        }

        board.addMark(row, col, ' ');
        board.markCountDecrease(2);
        return false;
    }

}
